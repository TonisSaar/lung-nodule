% function for LIDC database crawling and data extraction
% Data will be stored under lidc variable
addpath xml_toolbox
lidcFolder = 'H:\LIDC';

dirs = regexp(genpath(lidcFolder),['[^;]*'],'match');
folderVec = [];
for i = 1:numel(dirs)
    if(~isempty(strfind(dirs{i}, '\000000')))
        folderVec{end+1} = dirs{i};
    end
end

lidc = [];
for i = 1:numel(folderVec)
    i
    [I, Im, index, ImSize, ImSpacing] = readDicom(folderVec{i});
    nodules = readXML(folderVec{i});
    if ~isempty(nodules)
        lidc(end+1).folder = folderVec{i};
        lidc(end).data = nodules;
        lidc(end).size(1:3) = ImSize;
        lidc(end).spacing(1:3) = ImSpacing;
        lidc(end).index= index;
    end
end