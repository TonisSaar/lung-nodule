function [sizemat]= add_features(noduleVol,mit, x, y, z,blobImg,blobVol,resizedNoduleVol )
addpath add_newfeatures
xtt=(noduleVol{mit}{1,3});
xt=xtt(1,1);
yt=xtt(1,2);
zt=xtt(1,3);
xnew=x*xt; ynew=y*yt; znew=z*zt;
numberVoxel = sum(blobImg(:));
volume_1 = numberVoxel * xt * yt * zt;
ty=nthroot(volume_1,3);
solidity = getSolidity(blobImg,xt,zt);
[ROIonly,levels] = prepareVolume(blobVol,blobImg,'CTscan',xt,zt,1,'pixelW','Matrix','Lloyd',32);
[GLCM] = getGLCM(ROIonly,levels);
[textures] = getGLCMtextures(GLCM);
[~,~,SUVmean,aucCSH] = getSUVmetrics(resizedNoduleVol(:));
sizemat=[xnew ynew znew ty  solidity SUVmean aucCSH textures.Energy textures.Contrast textures.Entropy textures.Homogeneity textures.Correlation textures.SumAverage textures.Variance textures.Dissimilarity];
end