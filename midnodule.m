% %%% for nodules >6& nodules<=10mm %%%
function n3=midnodule(numberToExtract3) 
 midnodule =  numberToExtract3;
  ts= strel('disk',2);
    ts1 = imerode(midnodule,ts);

[B1,L1] = bwboundaries(ts1,'noholes');
labeledImageMidNodules=bwlabel(ts1);
statsMid = regionprops(L1,ts1,'all');

metricMid=size(B1);
metricMid1=size(metricMid);
for kM = 1:length(B1)
    perimeter = FacetMidpointPerim(kM);
    % %     % obtain the area calculation corresponding to label 'k'
    area = statsMid(kM).Area;
    % %     % compute the roundness metric
    %metricMid(kM)=(4*pi*area)/perimeter ^2;
    % %     % compute the Eccentricity metric
    %metriceMid(kM) = statsMid(kM).Eccentricity;
    %%% compactness
    metricMid(kM) = perimeter^2/(4*pi*area);
    %%elognation
    metricMid1(kM) = statsMid(kM).MajorAxisLength/statsMid(kM).MinorAxisLength;    
end
allBlobroundnessMid = metricMid;

allBlorroundnessMid1=metricMid1;
allowablerIndexesMid = (allBlobroundnessMid>0.0150);%&(allBlobroundnessMid>0.0150);%&(allBlobroundnessMid<0.0500);%
keeperIndexesMid = find(allowablerIndexesMid );
keeperBlobsImageMid = ismember(labeledImageMidNodules, keeperIndexesMid);

% labeleImageMid=bwlabel(keeperBlobsImageMid);
% % %allowedIndexes3 =  %(allBlobround3 >0.40)&(allBlobround3 >0.05)&(allBlobround3 <0.30)+((allBlobround3 >0.40)&(allBlobround3 <1.8);
%  allowedIndexesMid = (allBlorroundnessMid1>1.2)&(allBlorroundnessMid1 <2.6);%(allBlorroundnessMid1>1.6);%&(allBlorroundnessMid1 <6.99);
%  keeperIndexMid1 = find(allowedIndexesMid);

%keeperBlobsImageMid1 = ismember(labeleImageMid,  keeperIndexMid1);
n3=keeperBlobsImageMid;
%midnodule_fn= midnodule;%keeperBlobsImageMid1;
end
 