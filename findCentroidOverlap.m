%visualize overlapping nodules as 3D volumes
clc;
close all;
tic;
load('lidc5.mat')
set = lidc(486)
overlapTol = 3;  %overlap tolerance in x and y direction


%join the enviroment varialbe with the relative path
set.folder = [getenv('LIDC') set.folder];

[~, I, ~, ~, ~] =readDicom(set.folder);

% if parpool('local') == 0 % checking to see if my pool is already open
%     parpool open 8 
% end

F = false(size(I));
for i = 1:size(I,3)
    [F(:,:,i), maskedImage] = lungAndNoduleExtract( set, I(:,:,i));
end
[L, num] = bwlabeln(F);
blobImages = regionprops(L, 'Image');

for i = 1:size(set.centroids,1)
    figure(i)
    [Ff, maskedImage] = lungAndNoduleExtract( set, I(:,:,round(set.centroids(i,3))));
    imshow(maskedImage, []);
    hold on
    [x,y] = find(F(:,:,round(set.centroids(i,3))));
    plot(y,x,'b.');
    plot(round(set.centroids(i,2)),round(set.centroids(i,1)),'r-x', 'MarkerSize',15)
    hold off
end

noduleBB = [];
for i = 1:size(set.centroids,1)
    label = L((round(set.centroids(i,1))-overlapTol):1:(round(set.centroids(i,1))+overlapTol),...
        (round(set.centroids(i,2))-overlapTol):1:(round(set.centroids(i,2))+overlapTol),round(set.centroids(i,3)));
    label = max(label(:));
    if (label > 0)
        figure()
        blobImg = blobImages(label).Image;
%        s=strel('disk',10);
%        blobImg=imerode(sp(:,:,:),s);
        if numel(blobImg) < 2
            continue
        end
        
        %add padding of zeros around the nodule
        blobImg = padarray(blobImg,[3 3 3]);
        
        [m,n,p] = size(blobImg);
        [X,Y,Z] = meshgrid(1:n,1:m,1:p);
        p1 = patch(isosurface(X,Y,Z,blobImg, 0),...
            'FaceColor','red','EdgeColor','none');
        isonormals(X,Y,Z,blobImg,p1);
        p1.FaceColor = 'red';
        p1.EdgeColor = 'none';
        daspect([1,1,1])
        view(3); axis tight
        camlight
        lighting gouraud
    end
end

elapsedTime = toc;

message = sprintf('\nElapsed time = %.2f seconds.', elapsedTime)

%[x,y,z,D] = subvolume(I,[minBB(1),maxBB(1),minBB(2),maxBB(2),minBB(3),maxBB(3)]);

% p2 = patch(isocaps(x,y,z,D, 5),...
%      'FaceColor','interp','EdgeColor','none');

