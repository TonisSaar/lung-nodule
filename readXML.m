function nodules = readXML(foldername)

xmlList = dir(fullfile(foldername,'*.xml'));
nodules = [];
if numel(xmlList) < 1
    return
end


for xmlNr = 1 : numel(xmlList)
    xDoc = xml_load([foldername '\' xmlList(xmlNr).name]);
    
    for i = 1:size(xDoc,2)
        for j = 1:size(xDoc(1,i).readingSession,2)
            radiol = '';
            radiol = xDoc(1,i).readingSession(1,1).servicingRadiologistID;
            
            if isfield(xDoc(1,i).readingSession(1,j),'unblindedReadNodule')
                if ~isempty(xDoc(1,i).readingSession(1,j).unblindedReadNodule)
                    nodule = xDoc(1,i).readingSession(1,j).unblindedReadNodule(1,1).noduleID;
                    nodules(end + 1).nodule = nodule;
                    nodules(end).radiologist = radiol;
                    nodules(end).coord = [];
                    readSize = size(xDoc(1,i).readingSession(1,j).unblindedReadNodule,2);
                    for l = 1:readSize
                        zPos = xDoc(1,i).readingSession(1,j).unblindedReadNodule(1,l).roi(1,1).imageZposition;
                        pointCount = size(xDoc(1,i).readingSession(1,j).unblindedReadNodule(1,l).roi,2);
                        
                        %imshow(I{find(index == str2double(zPos))},[],'InitialMagnification','fit','Border','tight')
                        %hold on
                        for k = 1:pointCount
                            x = str2double(xDoc(1,i).readingSession(1,j).unblindedReadNodule(1,l).roi(1,k).edgeMap(1,1).xCoord);
                            y = str2double(xDoc(1,i).readingSession(1,j).unblindedReadNodule(1,l).roi(1,k).edgeMap(1,1).yCoord);
                            %plot(x, y, 'r.');
                            nodules(end).coord = [ nodules(end).coord; x y str2double(zPos)];
                        end
                        
%                         text(10,10,['Radiologist: ' radiol],'color','yellow');
%                         text(10,28,['Nodule: ' nodule],'color','yellow');
%                         text(10,46,['Z: ' zPos],'color','yellow');
%                         hold off
                        filename = '';
                        filename = strcat(nodule,'-', zPos);
                        filename = strcat(filename , '.jpg');
                        %saveas(gcf,['images/' filename], 'jpg')
                    end
                end
            end
        end
    end
end
end