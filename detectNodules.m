mkdir('images')
mkdir('nn_images')
for index_new = 1:1%numel(lidc)
    set = lidc(index_new);
    index_new
    %set = lidc(1);
    set.folder = [getenv('LIDC') set.folder];
    [tmp1 I tmp1 tmp1 tmp1] =readDicom(set.folder);
    clear tmp1;
    
    F = false(size(I));
    for i = 1:size(I,3)
        [F(:,:,i) maskedImage] = lungAndNoduleExtract(set, I(:,:,i));
    end
    
    [L num] = bwlabeln(F);
    blobImages = regionprops(L, 'Image', 'BoundingBox','Centroid');
    tmpp3=[];
    p1=[];
    index = 1;
    all_can=[];
    %ipm=0;
    nodCount  = 0;
    nonnodCount=0;
    fid = fopen ('C:\Users\Anindya.PC\Documents\lung-nodule\images\lidcnew_n.txt','a+') ;
    fprintf(fid,'LIDC_set #%2d\n',index_new);
    fprintf(fid,'nodule #      Centroid_X  Centroid_Y    Centroid_Z    diameter\n');
    fid1 = fopen ('C:\Users\Anindya.PC\Documents\lung-nodule\nn_images\lidcnew_nn.txt','a+') ;
    fprintf(fid1,'LIDC_set #%2d\n',index_new);
    fprintf(fid1,'nodule #      Centroid_X  Centroid_Y    Centroid_Z    diameter\n');
    for i = 1:num
        nodule = {};
        bbVol = round(blobImages(i).BoundingBox);
        nodule{index}{i,1} = I(bbVol(2):bbVol(2)+bbVol(5)-1,bbVol(1):bbVol(1)+bbVol(4)-1,bbVol(3):bbVol(3)+bbVol(6)-1);
        blobImg = blobImages(i).Image;
        nodule{index}{i,2} = blobImg;
        nodule{index}{1,3} = set.spacing;
        
        [volRes  tmp1] = resizeNodules(nodule,[10 10 5],true,false);
        
        clear tmp1;
        if ~isempty(volRes)
            all_can(:,end+1)=(volRes(1,:));
            p=volRes(1,504);
            if (p>=6)
                
                pq=volRes(1,:);
                p1(:,end+1)=pq;%(volRes(1,:));
%                 class = predict(trainedClassifier1, pq);
                
                                test = results.net(pq');
                                testVec(i,:) = test;
                                [tmp1 class] = max(test);
                                clear tmp1;
                if (class == 1)
                    nodCount = nodCount + 1;
                    im = I(:,:,round(blobImages(i).Centroid(3)));
                    fig=figure(1);
                    imshow(im,[])
                    hold on
                    plot(round(blobImages(i).Centroid(1)),round(blobImages(i).Centroid(2)),'r-x', 'MarkerSize',15)
                    hold off
                    print(fig,['images/'  num2str(index_new) '_' num2str(round(blobImages(i).Centroid(1))) '_' num2str(round(blobImages(i).Centroid(2))) '_' num2str(round(blobImages(i).Centroid(3)))],'-dpng')
                    fprintf(fid,'#%2d %17.1f %11.1f %13.1f %12.1f\n', nodCount, (round(blobImages(i).Centroid(1))), (round(blobImages(i).Centroid(2))), (round(blobImages(i).Centroid(3))), p);
                else
                    nonnodCount = nonnodCount + 1;
                    im = I(:,:,round(blobImages(i).Centroid(3)));
                    fig=figure(1);
                    imshow(im,[])
                    hold on
                    plot(round(blobImages(i).Centroid(1)),round(blobImages(i).Centroid(2)),'r-x', 'MarkerSize',15)
                    hold off
                    print(fig,['nn_images/'  num2str(index_new) '_' num2str(round(blobImages(i).Centroid(1))) '_' num2str(round(blobImages(i).Centroid(2))) '_' num2str(round(blobImages(i).Centroid(3)))],'-dpng')
                    fprintf(fid1,'#%2d %17.1f %11.1f %13.1f %12.1f\n', nonnodCount, (round(blobImages(i).Centroid(1))), (round(blobImages(i).Centroid(2))), (round(blobImages(i).Centroid(3))), p);
                end
            end
            tmpp3=p1';
            % end
        end
    end
    nodCount
    nonnodCount
    fclose(fid) ;
    fclose(fid1) ;
end

