function[count] = testCentroidOverlap(set, overlapTol)

count = 0;
%join the enviroment varialbe with the relative path
set.folder = [getenv('LIDC') set.folder];

[~, I, ~, ~, ~] =readDicom(set.folder);

F = false(size(I));
for i = 1:size(I,3)
    [F(:,:,i), maskedImage] = lungAndNoduleExtract(set, I(:,:,i));
end
[L, num] = bwlabeln(F);
%blobImages = regionprops(L, 'Image');

% for i = 1:size(set.centroids,1)
%     figure(i)
%     [Ff, maskedImage] = noduleExtract( I(:,:,round(set.centroids(i,3))));
%     imshow(maskedImage, []);
%     hold on
%     [x,y] = find(F(:,:,round(set.centroids(i,3))));
%     plot(y,x,'b.');
%     plot(round(set.centroids(i,2)),round(set.centroids(i,1)),'r-x', 'MarkerSize',15)
%     hold off
% end


for i = 1:size(set.centroids,1)
    label = L((round(set.centroids(i,1))-overlapTol):1:(round(set.centroids(i,1))+overlapTol),...
        (round(set.centroids(i,2))-overlapTol):1:(round(set.centroids(i,2))+overlapTol),round(set.centroids(i,3)));
    label = max(label(:));
    if (label > 0)
        count = count + 1;
    end
end
end