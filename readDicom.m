function [I Im index ImSize ImSpacing] = readDicom(foldername)
% I     -cell containing dicom layers
% Im    -3d array of dicom data
% index -contains vector of layer order
% ImSize -is the dataset size
% ImSpacing -vector of 3 layer spacings in x,y and z direction 

dicomlist = dir(fullfile(foldername,'*.dcm'));
if isempty(dicomlist)
    sprintf('ERROR: No DICOM files found in folder %s',foldername)
    return
end

I = [];
Im = [];
if numel(dicomlist) > 0
    Im = int16(zeros(512,512,1,numel(dicomlist)));
    I = cell(1,numel(dicomlist));
    I(:) = {int16(zeros(512, 512))};
    index = zeros(numel(dicomlist),1);
    instance = zeros(numel(dicomlist),1);
    for cnt = 1 : numel(dicomlist)
        info = dicominfo(fullfile(foldername,dicomlist(cnt).name));
        ImSpacing(1:2) = info.PixelSpacing;
        ImSpacing(3) = info.SliceThickness;
        ImSize(1) = info.Width;
        ImSize(2) = info.Height;
        index(cnt) = info.ImagePositionPatient(3);
        instance(cnt) = info.InstanceNumber;
        I{cnt} = int16(dicomread(fullfile(foldername,dicomlist(cnt).name)));
        ImSize(3) = numel(dicomlist);
    end
    
    
    indexSort = sort(index);
    for cnt = 1 : numel(dicomlist)
        Im(:,:,cnt) = I{indexSort(cnt) == index};
    end
    Im = squeeze(Im);
    index = indexSort;
end