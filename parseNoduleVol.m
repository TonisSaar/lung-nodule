%script to visualize nodules in nonNoduleVol.mat cell 
%nodule images will be saved to folder "/images/"
mkdir('images') 
 for mit = 1:numel(noduleVol)
     for nit = 1: size(noduleVol{mit},1)
         blobImg = [];
         blobImg = noduleVol{mit}{nit,2}; 
         if ~isempty(blobImg) 
             if numel(blobImg) < 2 
                 continue
             end
             %add padding of zeros around the nodule
             blobImg = padarray(blobImg,[3 3 3]); 
             clf 
             fig = figure(1); 
             %imshow(blobImg(:,:,ceil(size(blobImg,3)/2)),[])
             [m,n,p] = size(blobImg); 
             [X,Y,Z] = meshgrid(1:n,1:m,1:p); 
             p1 = patch(isosurface(X,Y,Z,blobImg, 0),... 
                 'FaceColor','red','EdgeColor','none'); 
             isonormals(X,Y,Z,blobImg,p1);
             p1.FaceColor = 'red';
             p1.EdgeColor = 'none'; 
             daspect([1,1,1]) 
             view(3); axis tight
             camlight 
             lighting gouraud 
             print(fig,['images/' num2str(mit) '- ' num2str(nit) ... 
                 'nodule'],'-dpng')
         end
     end
 end