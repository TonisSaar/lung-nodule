% %% for plotting PR curve and ROC curve and calculating the AUC-PR and AUC-ROCof test class
p1=[];
s_output=[];
s_target=[];
for n=1:numel(results1.info.test.indices)
    p=  results1.info.test.indices(n);
    p1(:,end+1)=p;
    s1=results1.output(:,p);
    s_output(:,end+1)=s1;
    s2=results1.target1(:,p);
    s_target(:,end+1)=s2;
end
% plot confusion matrix of test class
plotconfusion(s_target,s_output)
[c,cm,ind,per] = confusion(s_target,s_output);
% % 
% %% plot ROC curve and find AUC-ROC of test class
% x1=s_output(1,:);
% x2=s_output(2,:);
% y1=s_target(1,:);
% y2=s_target(2,:);
% t = y1';%[1 1 0 1 1 1 0 0 1 0 1 0 1 0 0 0 1 0 1 0]';
% y = x1';%[.9  .8  .7  .6  .55 .54 .53 .52 .51 .505 ...
% %.4  .39 .38 .37 .36 .35 .34 .33 .3 .1]';
% t1 = y2';%[1 1 0 1 1 1 0 0 1 0 1 0 1 0 0 0 1 0 1 0]';
% y3 = x2';
% [tp,fp] = roc(t,y);
% [tp1,fp1]=roc(t1,y3);
% figure(1);
% clf;
% plot(fp,tp,'b','LineWidth', 1.5);
% hold
% plot(fp1,tp1,'r','LineWidth', 1.5);
% xlabel('false positive rate');
% ylabel('true positive rate');
% title(['Area Under ROC curve (AUC: ' num2str(auroc(tp,fp)) ')' ,' Area Under ROC curve (AUC: ' num2str(auroc(tp1,fp1)) ')']);
% legend('nodule','non-nodule','Location','SouthEast');
% hold off
% grid on
% %%compute the area under the ROC
% 
% fprintf(1, 'AUROC   = %f\n', auroc(tp,fp));
% fprintf(1, 'AUROC1   = %f\n', auroc(tp1,fp1));
% 
% %% plot PR curve and find AUC-PR of test class
% [Xpr,Ypr,Tpr,AUCpr] = perfcurve(y1,x1, 1, 'xCrit', 'reca', 'yCrit', 'ppv');
% plot(Xpr,Ypr,'b','LineWidth', 1.5)
% xlabel('recall'); ylabel('Precision')
% 
% hold
% [Xpr1,Ypr1,Tpr1,AUCpr1] = perfcurve(y2, x2, 1, 'xCrit', 'reca', 'yCrit', 'prec');
% plot(Xpr1,Ypr1,'r','LineWidth', 1.5)
% title(['Precision-recall curve (AUC: ' num2str(AUCpr) ') ' 'Precision-recall curve (AUC: ' num2str(AUCpr1) ') '])
% legend('nodule','non-nodule','Location','SouthWest');
% hold off
% grid on
% fprintf(1, 'AUROC   = %f\n',AUCpr);
% fprintf(1, 'AUROC1   = %f\n', AUCpr1);