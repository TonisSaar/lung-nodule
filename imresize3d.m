function [Vnew] = imresize3d(V,scale,tsize,ntype,npad)
% -------------------------------------------------------------------------
% function [Vnew] = imresize3D(V,scale,tsize,ntype,npad)
% -------------------------------------------------------------------------
% DESCRIPTION: 
% This function resizes a 3D input volume to new dimensions. It is adapted 
% from the original code of D. Kroon of Twente(July 2008) available at: 
% <http://www.mathworks.com/matlabcentral/fileexchange/21451-multimodality-non-rigid-demon-algorithm-image-registration/content//functions/imresize3d.m>
% -------------------------------------------------------------------------
% INPUTS:
% - V: The input volume (3D array).
% - scale: scaling factor, when used set tsize to [].
% - tsize: new dimensions, when used set scale to [].
% - ntype: Type of interpolation ('nearest', 'linear', or 'cubic')
% - npad: Boundary condition ('replicate', 'symmetric', 'circular', 'fill', 
%         or 'bound')  
% -------------------------------------------------------------------------
% OUTPUTS:
% - Vnew: Resized volume.


% Check the inputs
if(exist('ntype', 'var') == 0), ntype='nearest'; end
if(exist('npad', 'var') == 0), npad='bound'; end
if(exist('scale', 'var')&&~isempty(scale)), tsize=round(size(V)*scale); end
if(exist('tsize', 'var')&&~isempty(tsize)),  scale=(tsize./size(V)); end

% Make transformation structure  
%blob_center = (size(V) + 1) / 2;
T = makehgtform('scale',scale);
tform = maketform('affine', T);

% Specify resampler
R = makeresampler(ntype, npad);

% Resize the image volume
Vnew = tformarray(V, tform, R, [1 2 3], [1 2 3], tsize, [], 0);
end