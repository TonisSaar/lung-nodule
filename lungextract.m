function [lungnew] = lungextract( grayImage)
%grayImage=dicomread('C:\Users\Anindya.PC\Desktop\lung\LIDC-IDRI-0013\1.3.6.1.4.1.14519.5.2.1.6279.6001.331662654015358587276208254750\000000\000002');
[rows, columns ] = size(grayImage);
binaryImage = grayImage<400;
%tp=binaryImage - imclearborder(binaryImage);
borderImage = logical(binaryImage - imclearborder(binaryImage));
I2 = imsubtract(binaryImage, borderImage);
%figure, imshow(I2,[]);
[labeledImage, numberOfBlobs] = bwlabel(I2);
blobMeasurements = regionprops(labeledImage, 'area');
allAreas = [blobMeasurements.Area];
[~, sortIndices] = sort(allAreas, 'descend');
if (numel(sortIndices)>1)
    blob1=(sortIndices(1));
    keeperBlobsImage1 = ismember(labeledImage, blob1);
    binaryImage23 = imfill(keeperBlobsImage1>0, 'holes'); %E1
    %%for right lung blob
    blob2=(sortIndices(2));
    keeperBlobsImage2 = ismember(labeledImage, blob2);
    e = strel('disk',20);
    E = imclose(keeperBlobsImage2,e);
    %initalize for left blob
    lung = zeros(rows, columns, 'uint16'); % Initialize.
    % initalize for right blob
    lung1 = zeros(rows, columns, 'uint16'); % Initialize.
    % extracted left lung
    lung(binaryImage23) = grayImage(binaryImage23);
    % extracted right lung
    lung1(E) = grayImage(E);
    lungt=imadd(lung,lung1);
else% (numel(sortIndices)>1)
    if (numel(sortIndices)==1)
        blob1=(sortIndices(1));
        keeperBlobsImage1 = ismember(labeledImage, blob1);
        binaryImage23 = imfill(keeperBlobsImage1>0, 'holes');
        lung = zeros(rows, columns, 'uint16'); % Initialize.
        lung(binaryImage23) = grayImage(binaryImage23);
        lungt=lung;
    else
        blob1=(sortIndices);
        keeperBlobsImage1 = ismember(labeledImage, blob1);
        binaryImage23 = imfill(keeperBlobsImage1>0, 'holes');
        lung = zeros(rows, columns, 'uint16'); % Initialize.
        lung(binaryImage23) = grayImage(binaryImage23);
        lungt=lung;
    end
    
end
lungnew=lungt;
end