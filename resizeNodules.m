function [noduleVolRes, noduleBlbRes] = resizeNodules(noduleVol,sizeArray,add_xyz,triple)
%function to resize nodules
%
xsize = sizeArray(1);
ysize = sizeArray(2);
zsize = sizeArray(3);
resizedNoduleVol = [];
noduleVolRes = single([]);
noduleBlbRes = int16([]);

for mit = 1:numel(noduleVol)%271:400%
    mit
    for nit = 1: size(noduleVol{mit},1)
        blobImg = [];
        blobImg = int16(noduleVol{mit}{nit,2});
        blobVol = [];
        blobVol = int16(noduleVol{mit}{nit,1});
        
        if ~isempty(blobImg)
            if (numel(size(blobImg)) >= 3)
                [x, y, z] = size(blobVol);
                xtt=(noduleVol{mit}{1,3});
                xt=xtt(1,1);
                yt=xtt(1,2);
                zt=xtt(1,3);
                xnew=x*xt; ynew=y*yt; znew=z*zt;
                numberVoxel = sum(blobImg(:));
                volume_1 = numberVoxel * xt * yt * zt;
                ty=nthroot(volume_1,3);
                if (ty>=6)
                    resizedNoduleVol = imresize3d(blobVol,[],[ysize xsize zsize],'nearest','fill');
                    resizedNoduleVol=single(resizedNoduleVol);
                    resizedBlbVol = imresize3d(blobImg,[],[ysize xsize zsize],'nearest','fill');
                    resizedBlbVol=single(resizedBlbVol);
                    % end
                    if (add_xyz==true)
                        noduleVolRes_1 = int16(resizedNoduleVol(:)');
                        sizemat= add_features(noduleVol,mit, x, y, z,blobImg,blobVol,resizedNoduleVol(:));
                        noduleVolRes_1 =  [single(noduleVolRes_1) single(sizemat)];%int16(resizedNoduleVol(:));
                        noduleVolRes(end+1,:) = (noduleVolRes_1(:));
                        noduleBlbRes(end+1,:) =  int16(resizedBlbVol(:));
                        %use triple flag to mirror the data on three axis
                    end
                    
                    if (triple == true)
                        if (add_xyz==true)
                            tmp = resizedNoduleVol(end:-1:1,:,:);
                            noduleVolRes_1 = int16(tmp(:)');
                            sizemat= add_features(noduleVol,mit, x, y, z,blobImg,blobVol,tmp(:));
                            noduleVolRes_1 =  [single(noduleVolRes_1) single(sizemat)];
                            noduleVolRes(end+1,:) =  (noduleVolRes_1(:));
                            tmp = resizedBlbVol(end:-1:1,:,:);
                            noduleBlbRes(end+1,:) =  tmp(:);
                            
                            tmp = resizedNoduleVol(:,end:-1:1,:);
                            noduleVolRes_1 = int16(tmp(:)');
                            sizemat= add_features(noduleVol,mit, x, y, z,blobImg,blobVol,tmp(:));
                            noduleVolRes_1 =  [single(noduleVolRes_1) single(sizemat)];
                            noduleVolRes(end+1,:) =  (noduleVolRes_1(:));
                            tmp = resizedBlbVol(:,end:-1:1,:);
                            noduleBlbRes(end+1,:) =  tmp(:);
                            
                            tmp = resizedNoduleVol(:,:,end:-1:1);
                            noduleVolRes_1 = int16(tmp(:)');
                            sizemat= add_features(noduleVol,mit, x, y, z,blobImg,blobVol,tmp(:));
                            noduleVolRes_1 =  [single(noduleVolRes_1) single(sizemat)];
                            noduleVolRes(end+1,:) =  (noduleVolRes_1(:));
                            tmp = resizedBlbVol(:,:,end:-1:1);
                            noduleBlbRes(end+1,:) =  tmp(:);
                        end
                    end
                    
                else
                    continue;
                end
            end
        end
    end
end
end