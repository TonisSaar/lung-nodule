% %%% for nodules >2.95 & nodules<=6mm %%%
function n2=semimidnodule(numberToExtract2) 
   semimidnodule =  numberToExtract2;
   
   ts= strel('disk',1);
    ts1 = imerode(semimidnodule,ts);
    [B1,L1] = bwboundaries(ts1,'noholes');
labeledImageMidNodules=bwlabel(ts1);
statsMid = regionprops(L1,ts1,'all');

metricsemiMid=size(B1);
metricsemiMid1=size(metricsemiMid);
for kM = 1:length(B1)
    perimeter = FacetMidpointPerim(kM);
    % %     % obtain the area calculation corresponding to label 'k'
    area = statsMid(kM).Area;
    % %     % compute the roundness metric
    %metricMid(kM)=(4*pi*area)/perimeter ^2;
    % %     % compute the Eccentricity metric
    %metriceMid(kM) = statsMid(kM).Eccentricity;
    %%% compactness
    metricsemiMid(kM) = perimeter^2/(4*pi*area);
    %%elognation
    metricsemiMid(kM) = statsMid(kM).MajorAxisLength/statsMid(kM).MinorAxisLength;    
end
allBlobroundnesssemiMid = metricsemiMid;

allBlorroundnesssemiMid1=metricsemiMid;
allowablerIndexessemiMid = (allBlobroundnesssemiMid>0.0025);%&(allBlobroundnesssemiMid<0.0100);% before 0.0045
keeperIndexessemiMid = find(allowablerIndexessemiMid );
keeperBlobsImagesemiMid = ismember(labeledImageMidNodules, keeperIndexessemiMid);

% labeleImageMid=bwlabel(keeperBlobsImageMid);
% % %allowedIndexes3 =  %(allBlobround3 >0.40)&(allBlobround3 >0.05)&(allBlobround3 <0.30)+((allBlobround3 >0.40)&(allBlobround3 <1.8);
%  allowedIndexesMid = (allBlorroundnessMid1>1.2)&(allBlorroundnessMid1 <2.6);%(allBlorroundnessMid1>1.6);%&(allBlorroundnessMid1 <6.99);
%  keeperIndexMid1 = find(allowedIndexesMid);

%keeperBlobsImageMid1 = ismember(labeleImageMid,  keeperIndexMid1);
    n2=keeperBlobsImagesemiMid;
end
