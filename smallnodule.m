%%% for nodules <=2.95mm %%%
function n1=smallnodule(numberToExtract1)
 smallnodule =  numberToExtract1;
% smallnodule_fn=smallnodule;
%ts= strel('disk',1);
ts1 = bwmorph(smallnodule,'thicken');
[B3,L3] = bwboundaries(ts1,'noholes');%imthick
 labeledImagesmallNodules=bwlabel(ts1);
 statssmall = regionprops(L3,ts1,'all');
 
metricsmall=size(B3);
metricsmall1=size(metricsmall);
% metricsmall1=size(metricsmall);
for kS = 1:length(B3)
    perimeter = statssmall(kS).Eccentricity;
    % %     % obtain the area calculation corresponding to label 'k'
    area = statssmall(kS).Area;
%     % %     % compute the roundness metric
%     %metricMid(kM)=(4*pi*area)/perimeter ^2;
%     % %     % compute the Eccentricity metric
%     %metriceMid(kM) = statsMid(kM).Eccentricity;
%     %%% compactness
     metricsmall(kS) = area;% perimeter^2/(4*pi*area);
%     %%elognation
     metricsmall1(kS) = perimeter;%statssmall(kS).MajorAxisLength/statssmall(kS).MinorAxisLength;    
 end
 allBlobroundnesssmall = metricsmall;
%save('x.mat','allBlobroundnesssmall'); 

  %allBlorroundnesssmall1=metricsmall1;
%  save('x1.mat','allBlorroundnesssmall1'); 
 allowablerIndexessmall = (allBlobroundnesssmall>5)&(allBlobroundnesssmall <=30);%
 keeperIndexessmall = find(allowablerIndexessmall );
 keeperBlobsImagesmall = ismember(labeledImagesmallNodules, keeperIndexessmall);
%  labeleImagesmall=bwlabel(keeperBlobsImagesmall);
% % % % %allowedIndexes3 =  %(allBlobround3 >0.40)&(allBlobround3 >0.05)&(allBlobround3 <0.30)+((allBlobround3 >0.40)&(allBlobround3 <1.8);
%    allowedIndexessmall = (allBlorroundnesssmall1>0.65)&(allBlorroundnesssmall1 <0.75);
%    keeperIndexsmall1 = find(allowedIndexessmall);
% % % 
%   keeperBlobsImagesmall1 = ismember(labeleImagesmall,  keeperIndexsmall1);
sp=strel('disk',1);
sp1=imerode(keeperBlobsImagesmall,sp);
n1=sp1;%smallnodule;
end