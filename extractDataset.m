%gather nodule dataset from the LIDC data
%dataset will be saved to noduleVol cell
tic
close all;
load('lidc5.mat')
overlapTol = 3;  %overlap tolerance in x and y direction
noduleVol = {};
nonNoduleVol = {};

% if parpool('local') == 0 % checking to see if my pool is already open
%     parpool open 4
% end

parfor index = 1:numel(lidc)
    set = lidc(index);
    index
    set.folder = [getenv('LIDC') set.folder];
    [~, I, ~, ~, ~] =readDicom(set.folder);
    F = false(size(I));
    
    for i = 1:size(I,3)
        [F(:,:,i), maskedImage] = lungAndNoduleExtract( set, I(:,:,i));
    end
    
    [L, num] = bwlabeln(F);
    blobImages = regionprops(L, 'Image', 'BoundingBox');
    labelVec = [];
    
    for i = 1:size(set.centroids,1)
        label = L((round(set.centroids(i,1))-overlapTol):1:(round(set.centroids(i,1))+overlapTol),...
            (round(set.centroids(i,2))-overlapTol):1:(round(set.centroids(i,2))+overlapTol),round(set.centroids(i,3)));
        label = max(label(:));
        if (label > 0)
            labelVec(end +1) = label;
            
            bbVol = round(blobImages(label).BoundingBox);
            noduleVol{index}{i,1} = I(bbVol(2):bbVol(2)+bbVol(5)-1,bbVol(1):bbVol(1)+bbVol(4)-1,bbVol(3):bbVol(3)+bbVol(6)-1);
            blobImg = blobImages(label).Image;
            noduleVol{index}{i,2} = blobImg;
        end
        noduleVol{index}{1,3} = [set.spacing(1) set.spacing(2) set.spacing(3)];
    end
    
    %if
    %imsave(blobImg,[])  imshow(noduleVol{1,1}{3,2}(:,:,5),[])
end

rand_array = randperm(899);
parfor index = 1:numel(lidc)
    set = lidc(index);%(rand_array(index))
    index
    set.folder = [getenv('LIDC') set.folder];
    [~, I, ~, ~, ~] =readDicom(set.folder);
    F = false(size(I));
    
    for i = 1:size(I,3)
        [F(:,:,i), maskedImage] = lungAndNoduleExtract(set, I(:,:,i));
    end
    
    [L, num] = bwlabeln(F);
    blobImages = regionprops(L, 'Image', 'BoundingBox');
    labelVec = [];
    
    for i = 1:num
        if ~ismember(i,labelVec)
            bbVol = round(blobImages(i).BoundingBox);
            nonNoduleVol{index}{i,1} = I(bbVol(2):bbVol(2)+bbVol(5)-1,bbVol(1):bbVol(1)+bbVol(4)-1,bbVol(3):bbVol(3)+bbVol(6)-1);
            blobImg = blobImages(i).Image;
            nonNoduleVol{index}{i,2} = blobImg;
        end
        nonNoduleVol{index}{1,3} = [set.spacing(1) set.spacing(2) set.spacing(3)];
    end
end

toc