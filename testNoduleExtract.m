% test to measure ExtractNodule performance 
% five first LIDC scans + 5 random ones are tested 
rng('shuffle')
if matlabpool('size') == 0 % checking to see if my pool is already open
    matlabpool open 8
end
tic
load('lidc5.mat')
setIndex = [1:6 round(rand(1,6)*numel(lidc))];
count = zeros(numel(setIndex),1);
lidcCount = zeros(numel(setIndex),1);
parfor i =1:numel(setIndex)
    count(i) = testCentroidOverlap(lidc(setIndex(i)),2);
    lidcCount(i) = size(lidc(i).centroids,1);
end
sprintf('Selected LIDC datasets nr. ')
setIndex
sprintf('Found in each dataset percent of nodules')
(count./lidcCount)*100
sprintf('Found in total %i nodules out of %i LIDC nodules',sum(count(:)),...
sum(lidcCount(:)))
toc
matlabpool close