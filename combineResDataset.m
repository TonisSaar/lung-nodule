%combine dataset from the resized nodules
resize_array = [10 10 5];
% % % %
 [tmp1, tmp2] = resizeNodules(noduleVol,resize_array,true,false);
%dataset1 = [tmp1 tmp2];

% %%% comment the below dataset1 if above dataset1 is uncommented
 dataset1= tmp1;
 dataset1(:,end+1) = 1;
% % %
 [tmp3, tmp4] = resizeNodules(nonNoduleVol,resize_array,true,false);
% % % % % %dataset2 = [tmp3 tmp4];
dataset2 = tmp3;
dataset2(:,end+1) = 0;
% % % % % % %
% % % % % % %select equal amount of nodules and non-nodules
% % % % % % ind = randperm(size(dataset2,1));
% % % % % % ind = ind(1:10*size(dataset1,1));
% % % % % % %
dataset3 = [dataset1;dataset2];
dataset4 = dataset3(randperm((size(dataset3,1))),:);
% % % % % % % % % % %
     data = double(dataset4(:,1:end-1));
% % % % % % % mu = mean(data);
% % % % % % % X_norm = bsxfun(@minus, data, mu);
% % % % % % % sigma = std(X_norm);
% % % % % % % data = bsxfun(@rdivide, X_norm, sigma);
clear target;
target(:,1) = dataset4(:,end);
target(:,2) = ~dataset4(:,end);
% % %dataset for external classifier
% %  dataNew = [target(:,1) data];

