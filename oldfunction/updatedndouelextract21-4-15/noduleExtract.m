function[nodule] =noduleExtract(grayImage)

[rows, columns] = size(grayImage);
D = im2bw(grayImage,0.005);

binaryImage = imfill(D, 'holes');
[labeledImage, numberOfBlobs] = bwlabel(binaryImage);
blobMeasurements = regionprops(labeledImage, 'area');

numberToExtract=(round(numberOfBlobs/1.1));
numberToExtract1=numberOfBlobs-numberToExtract;

numberToExtract = -numberToExtract;
smallnodule = Extractnodule(binaryImage, numberToExtract,labeledImage,blobMeasurements);

e = strel('disk',1);
E = imdilate(smallnodule,e);
[M,U] = bwboundaries(E,'noholes');
labeleImage=bwlabel(E);
stat = regionprops(U,E,'area','EquivDiameter','Eccentricity');
metrics=zeros(1, 40000);
metrics1=zeros(1, 150);
metrice1=zeros(1, 150);
for H = 1:length(M)
    boundary = M{H};
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    area = stat(H).Area;
    %equidiameter
    metrics(H) =sqrt(4 * area / pi);
    %circularity or roundness
    metrics1(H)= 4*pi*area/perimeter^2;
    % compute the Eccentricity metric
    metrice1(H) = stat(H).Eccentricity;
end
allBlobround =metrics;

allBlobround1 = metrics1;
allBlobround2=metrice1;

if max(allBlobround) <11%|| max(allBlobround) >13
    allowedIndexes = (allBlobround >5.5)&(allBlobround <8);
    keeperIndex = find(allowedIndexes);
    keepBlobImage1 = ismember(labeleImage, keeperIndex);
    labeleImage1=bwlabel(keepBlobImage1);
    %allowedIndexes1 = ((allBlobround1>0.85) &(allBlobround1 <1.10))+((allBlobround1 <1.18)&(allBlobround1 <1.45))+(allBlobround1 >1.65);
    allowedIndexes1 = ((allBlobround1>0.85) &(allBlobround1 <1.45))+(allBlobround1 >1.65);
    keeperIndex1 = find(allowedIndexes1 );
    keepBlobImage2 = ismember(labeleImage1, keeperIndex1);
    labeleImage2=bwlabel(keepBlobImage2);
    allowedIndexes2 = (allBlobround2 <0.20)+(allBlobround2 >0.50)&(allBlobround2 <0.85);
   keeperIndex2 = find(allowedIndexes2);
else
    allowedIndexes = (allBlobround >3.5)&(allBlobround <10);
    keeperIndex = find(allowedIndexes);
    keepBlobImage1 = ismember(labeleImage, keeperIndex);
    labeleImage1=bwlabel(keepBlobImage1);
    allowedIndexes1 = ((allBlobround1 >0.85)&(allBlobround1 <1.45))+((allBlobround1 >1.70));
    %allowedIndexes1 = ((allBlobround1 >0.85)&(allBlobround1 <1.45))+((allBlobround1 >1.70)&(allBlobround1 <1.75));
    %%allowedIndexes1 = ((allBlobround1 >1.25)&(allBlobround1 <1.30))+((allBlobround1 >1.70));
    keeperIndex1 = find(allowedIndexes1 );
    keepBlobImage2 = ismember(labeleImage1, keeperIndex1);
    labeleImage2=bwlabel(keepBlobImage2);
    allowedIndexes2 = (allBlobround2 <0.15)+((allBlobround2 >0.52)&(allBlobround2 <1));
    %allowedIndexes2 = (allBlobround2 <0.15)+((allBlobround2 >0.60)&(allBlobround2 <0.70));%&
    keeperIndex2 = find(allowedIndexes2);
%end
end
keepBlobImage = ismember(labeleImage2, keeperIndex2);

largeBlob = Extractnodule(binaryImage, numberToExtract1,labeledImage,blobMeasurements);
maskImage = zeros(rows, columns, 'uint16'); % Initialize.
maskImage(largeBlob) = grayImage(largeBlob);
dm = im2bw(maskImage,0.007);
ls= strel('disk',1);
tq = imerode(dm,ls);
[B,L] = bwboundaries(tq,'noholes');
labeledImage=bwlabel(tq);
%labeleImage3=bwlabel(tq);
stats = regionprops(L,tq,'area','Eccentricity');
metric=zeros(1, 150);
metrice=zeros(1, 150);
for k = 1:length(B)
    % obtain (X,Y) boundary coordinates corresponding to label 'k'
    boundary = B{k};
    % compute a simple estimate of the object's perimeter
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    % obtain the area calculation corresponding to label 'k'
    area = stats(k).Area;
    % compute the roundness metric
    metric(k) = 4*pi*area/perimeter^2;
    % compute the Eccentricity metric
    metrice(k) = stats(k).Eccentricity;
end
allBlobroundness = metric;

allBlobround3= metrice;
%save('x.mat','allBlobround3');
allowablerIndexes = (allBlobroundness >0.40);%&(allBlobroundness <1.48))+(allBlobroundness >1.75);%(allBlobroundness <0.05)& (allBlobroundness <0.75))+((allBlobroundness >0.85)&
keeperIndexes = find(allowablerIndexes );
%keeperIndex3=find(allowablerIndexes );
keeperBlobsImage1 = ismember(labeledImage, keeperIndexes);
labeleImage3=bwlabel(keeperBlobsImage1);
allowedIndexes3 = (allBlobround3 <1); %(allBlobround3 >0.40)&(allBlobround3 >0.05)&(allBlobround3 <0.30)+((allBlobround3 >0.40)&
%allowedIndexes3 = (allBlobround3 >0.20)&(allBlobround3 <0.55);
keeperIndex3 = find(allowedIndexes3 );
keeperBlobsImage = ismember(labeleImage3, keeperIndex3);

X=keeperBlobsImage;
Ft=imadd(keepBlobImage,X);
% binaryImage3 = imfill(Ft>0, 'holes');
% maskerImage = zeros(rows, columns, 'uint16'); % Initialize.
% maskerImage( binaryImage3) = grayImage( binaryImage3);
% labImage=bwlabel(maskerImage);
% s = regionprops(binaryImage3,  maskerImage,'MaxIntensity');
% numObj = numel(s);
% met=zeros(1,80);
% for k = 1 :  numObj
%     met(k) =s(k).MaxIntensity;
% end
% allBlo=met;
% lowStd = find(allBlo>480);
% kperBlobsImage1 = ismember(labImage, lowStd);
% % po=strel('disk',2);
% % tq1= imopen(kperBlobsImage1,po);
% % [Y,L] = bwboundaries(tq1,'noholes');
% % labeledImage=bwlabel(tq1);
% % stats1 = regionprops(L,tq1,'all');
% % metric=zeros(1, 300);
% % for k = 1:length(Y)
% %     area = stats1(k).Area;
% %     % compute the equidiameter metric
% %         metric(k) =sqrt(4 * area / pi);
% % end
% % allBlobroundness = (metric);
% % allowablerIndexes = (allBlobroundness >1);%&(allBlobroundness <20);
% % keeperIndexes = find(allowablerIndexes );
% % keeperBlobsImage = ismember(labeledImage, keeperIndexes);
nodule=Ft;
end
%F=imadd(E,X,'uint16');



