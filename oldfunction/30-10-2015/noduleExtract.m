function[nodule] =noduleExtract(set,grayImage)

[rows, columns] = size(grayImage);
D = im2bw(grayImage,0.005);
binaryImage = imfill(D, 'holes');

[M1,U1] = bwboundaries(binaryImage,'noholes');
[labeledImage, numberOfBlobs] = bwlabel(binaryImage);
blobMeasurements = regionprops(U1,labeledImage, 'Area');
allarea=size(M1);%=zeros(1, 400);
mt=size(allarea);%zeros(1, 400);

for h1=1:length(M1)
    % boundary = M1{h1};
    allarea(h1)=blobMeasurements(h1).Area;
    tq= allarea(h1).*((set.spacing(1)*set.spacing(2)));
    mt(h1)=sqrt((4*tq)/pi);
end  
%end

n2=mt;

%%%%%%% small nodules%%%%%%%
% number1=(n2<=2.95);
% keeperIndex1 = find(number1);
% numberToExtract1 = ismember(labeledImage, keeperIndex1);
% smallnodule =  numberToExtract1;
% ts1_small = bwmorph(smallnodule,'thicken');
% % sp_small1=strel('disk',1);
% % sp1_small1=imdilate(ts1_small,sp_small1);
% [BSmall3,LSmall3] = bwboundaries(ts1_small,'noholes');%imthick
% labeledImagesmallNodules=bwlabel(ts1_small);
% statssmall = regionprops(LSmall3,ts1_small,'all');
% 
% metricsmall=size(BSmall3);
% metricsmall1=size(metricsmall);
% % metricsmall1=size(metricsmall);
% for kSmall = 1:length(BSmall3)
%     perimeter = statssmall(kSmall).Eccentricity;
%     area = statssmall(kSmall).Area;
%     metricsmall(kSmall) = area;% perimeter^2/(4*pi*area);
%     metricsmall1(kSmall) = perimeter;%statssmall(kS).MajorAxisLength/statssmall(kS).MinorAxisLength;
% end
% allBlobroundnesssmall = metricsmall;
% allowablerIndexessmall = (allBlobroundnesssmall>4)&(allBlobroundnesssmall <=32);%
% keeperIndexessmall = find(allowablerIndexessmall );
% keeperBlobsImagesmall = ismember(labeledImagesmallNodules, keeperIndexessmall);
% 
% sp_small=strel('disk',1);
% sp1_small=imerode(keeperBlobsImagesmall,sp_small);
% n1=int16(sp1_small);%smallnodule;

% % 
% %%%%%%% semimid nodules%%%%%%%
 number2=(n2>2.95)&(n2<=6);
keeperIndex2 = find(number2);
numberToExtract2 = ismember(labeledImage, keeperIndex2);
%n22=semimidnodule(numberToExtract2); 
semimidnodule =  numberToExtract2;
ts1_sm= strel('disk',1);
ts1_SMM = imerode(semimidnodule,ts1_sm);
[BSemimid1,LSemimid1] = bwboundaries(ts1_SMM,'noholes');
labeledImageMidNodules=bwlabel(ts1_SMM);
statsMid = regionprops(LSemimid1,ts1_SMM,'all');
metricsemiMid=size(BSemimid1);
for kSemiM = 1:length(BSemimid1)
    perimeter = FacetMidpointPerim(kSemiM);
    % %     % obtain the area calculation corresponding to label 'k'
    area = statsMid(kSemiM).Area;
    %%% compactness
    metricsemiMid(kSemiM) = perimeter^2/(4*pi*area);
    %%elognation
    metricsemiMid(kSemiM) = statsMid(kSemiM).MajorAxisLength/statsMid(kSemiM).MinorAxisLength;
end
allBlobroundnesssemiMid = metricsemiMid;
allowablerIndexessemiMid = (allBlobroundnesssemiMid>0.0025);%&(allBlobroundnesssemiMid<0.0100);% before 0.0045
keeperIndexessemiMid = find(allowablerIndexessemiMid );
keeperBlobsImagesemiMid = ismember(labeledImageMidNodules, keeperIndexessemiMid);
% sp_smallmid=strel('disk',1);
% sp1_smallmid=imerode(keeperBlobsImagesemiMid,sp_smallmid);
n22=int16(keeperBlobsImagesemiMid);
% 
% % %%%%%%% medium nodules%%%%%%%
% number3=(n2>6)&(n2<=10);
% keeperIndex3 = find(number3);
% numberToExtract3 = ismember(labeledImage, keeperIndex3);
% midnodule =  numberToExtract3;
% ts1_m= strel('disk',2);
% ts1_MM = imerode(midnodule,ts1_m);
% [Bmid1,Lmid1] = bwboundaries(ts1_MM,'noholes');
% labeledImageMidNodules=bwlabel(ts1_MM);
% statsMid = regionprops(Lmid1,ts1_MM,'all');
% metricMid=size(Bmid1);
% metricMid1=size(metricMid);
% for kMid = 1:length(Bmid1)
%     perimeter = FacetMidpointPerim(kMid);
%     % %     % obtain the area calculation corresponding to label 'k'
%     area = statsMid(kMid).Area;
%     %%% compactness
%     metricMid(kMid) = perimeter^2/(4*pi*area);
%     %%elognation
%     metricMid1(kMid) = statsMid(kMid).MajorAxisLength/statsMid(kMid).MinorAxisLength;
% end
% allBlobroundnessMid = metricMid;
% allowablerIndexesMid = (allBlobroundnessMid>0.0150);%&(allBlobroundnessMid>0.0150);%&(allBlobroundnessMid<0.0500);%
% keeperIndexesMid = find(allowablerIndexesMid );
% keeperBlobsImageMid = ismember(labeledImageMidNodules, keeperIndexesMid);
% n3=int16(keeperBlobsImageMid);
% % 
% % 
% % %%%%%%% Large nodules%%%%%%%
% % % 
% number4=(n2>10);
% keeperIndex4 = find(number4);
% numberToExtract4 = ismember(labeledImage, keeperIndex4);
% largenodule =  numberToExtract4;
% [rows, columns] = size(grayImage);
% masklImage = zeros(rows, columns, 'uint16'); % Initialize.
% masklImage(largenodule) = grayImage(largenodule);
% dm = im2bw(masklImage,0.007);
% tql=bwmorph(dm,'branchpoints');
% if   or(min(set.spacing)<=0.80,max(set.spacing)>1.50) %max(set.spacing)>=1.5%length(set.index)<= 140
%     ts1_l= strel('disk',2);
%     ts1_LN = imerode(tql,ts1_l);
%     [Blarge,Llarge] = bwboundaries(ts1_LN,'noholes');
%     labeledImage=bwlabel(ts1_LN);
%     stats = regionprops(Llarge,ts1_LN,'all');
%     metric=size(Blarge);%(1, 15);
%     % metrice=zeros(1, 150);
%     for klarge = 1:length(Blarge)
%         perimeter = ConvexPerim(klarge);
%         %     % obtain the area calculation corresponding to label 'k'
%         area = stats(klarge).Area;
%         %     % compute the roundness metric
%         metric(klarge)=(4*pi*area)/perimeter ^2;
%         %     % compute the Eccentricity metric
%         %     metrice(k) = stats(k).Eccentricity;
%     end
%     allBlobroundness = metric;
%     t=max(allBlobroundness);
%     if (t <=100)%|| max(allBlobround) >13 **
%         allowablerIndexes = (allBlobroundness>=9);
%         allowablerIndexes_UN = (allBlobroundness<9);
%     elseif (t>100)&&(t<=300)
%         allowablerIndexes = (allBlobroundness>=85);
%         allowablerIndexes_UN = (allBlobroundness<85);
%     elseif(t>300)&&(t<=500)
%         allowablerIndexes = (allBlobroundness>=190);
%         allowablerIndexes_UN= (allBlobroundness<190);
%     else
%         allowablerIndexes = (allBlobroundness>=380);
%         allowablerIndexes_UN = (allBlobroundness>100)&(allBlobroundness<340);
%     end
%     keeperIndexes = find(allowablerIndexes );
%     keeperIndexes_UN = find(allowablerIndexes_UN);
%     
%     keeperBlobsImage1 = ismember(labeledImage, keeperIndexes);
%     keeperBlobsImage1_UN = ismember(labeledImage, keeperIndexes_UN);
%     UN=keeperBlobsImage1_UN;
%     ls= strel('disk',2);
%     ts2 = imdilate(keeperBlobsImage1,ls);
%     ts12=imadd(UN,ts2);
% else
%     [Blarge,Llarge] = bwboundaries(tql,'noholes');
%     labeledImage=bwlabel(tql);
%     stats = regionprops(Llarge,tql,'all');
%     metric=size(Blarge);%(1, 15);
%     % metrice=zeros(1, 150);
%     for klarge = 1:length(Blarge)
%         perimeter = ConvexPerim(klarge);
%         %     % obtain the area calculation corresponding to label 'k'
%         area = stats(klarge).Area;
%         %     % compute the roundness metric
%         metric(klarge)=(4*pi*area)/perimeter ^2;
%         %     % compute the Eccentricity metric
%         %     metrice(k) = stats(k).Eccentricity;
%     end
%     allBlobroundness = metric;
%     t=max(allBlobroundness);
%     if (t <=100)%|| max(allBlobround) >13 **
%         allowablerIndexes = (allBlobroundness>=9);
%         allowablerIndexes_UN = (allBlobroundness<9);
%     elseif (t>100)&&(t<=300)
%         allowablerIndexes = (allBlobroundness>=85);
%         allowablerIndexes_UN = (allBlobroundness<85);
%     elseif(t>300)&&(t<=500)
%         allowablerIndexes = (allBlobroundness>=190);
%         allowablerIndexes_UN= (allBlobroundness<190);
%     else
%         allowablerIndexes = (allBlobroundness>=380);
%         allowablerIndexes_UN = (allBlobroundness>100)&(allBlobroundness<340);
%     end
%     keeperIndexes = find(allowablerIndexes );
%     keeperIndexes_UN = find(allowablerIndexes_UN);
%     
%     keeperBlobsImage1 = ismember(labeledImage, keeperIndexes);
%     keeperBlobsImage1_UN = ismember(labeledImage, keeperIndexes_UN);
%     UN=keeperBlobsImage1_UN;
%     ls= strel('disk',2);
%     tq1 = imdilate(keeperBlobsImage1,ls);
%     ts1_l= strel('disk',1);
%     ts2 = imerode(tq1,ts1_l);
%     ts12=imadd(UN,ts2);
% end
% n4=int16(ts12);%largenodule;
%   nset=imadd(n1,n22);
%   nset1=imadd(n3,n4);
%   nset_l= strel('disk',1);
%     nset2 = imerode(nset1,nset_l);
%  nset2=uint16(nset1);
nodule_com=n22;%imadd(nset,nset1);%imadd(n3,nset);%set;%imadd(nset,n1);%imadd(n1,nset);%n4+n3+n2;%+int16(n2)+int16(n3)+int16(n4);
%nodule_com= imlimbcomb(n1,n2,n3,n4);%large;%imadd(nset, nset2);%small;%mid;%large;%imadd(mid,large);% imadd(small, semimid, mid, large);   ;%

nodule=nodule_com;
end

