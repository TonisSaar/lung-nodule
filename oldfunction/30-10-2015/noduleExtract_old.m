function[nodule] =noduleExtract(set,grayImage)

[rows, columns] = size(grayImage);
D = im2bw(grayImage,0.005);
binaryImage = imfill(D, 'holes');

[M1,U1] = bwboundaries(binaryImage,'noholes');
[labeledImage, numberOfBlobs] = bwlabel(binaryImage);
blobMeasurements = regionprops(U1,labeledImage, 'Area');
allarea=size(M1);%=zeros(1, 400);
mt=size(allarea);%zeros(1, 400);

for h1=1:length(M1)
    % boundary = M1{h1};
    allarea(h1)=blobMeasurements(h1).Area;
    tq= allarea(h1).*((set.spacing(1)*set.spacing(2)));
    mt(h1)=sqrt((4*tq)/pi);
end  
%end

n2=mt;
% number1=(n2<=2.95);
% keeperIndex1 = find(number1);
% numberToExtract1 = ismember(labeledImage, keeperIndex1);
% 
% number2=(n2>2.95)&(n2<=6);
% keeperIndex2 = find(number2);
% numberToExtract2 = ismember(labeledImage, keeperIndex2);
% number3=(n2>6)&(n2<=10);
% keeperIndex3 = find(number3);
% numberToExtract3 = ismember(labeledImage, keeperIndex3);
number4=(n2>10);
keeperIndex4 = find(number4);
numberToExtract4 = ismember(labeledImage, keeperIndex4);

% small=smallnodule(numberToExtract1);%numberToExtract1;
% semimid=semimidnodule(numberToExtract2);
%mid=midnodule(numberToExtract3);
 large=largenodule(set, grayImage,numberToExtract4);
%K = imlincomb(1,small,1,J,'uint16')
%nset=imadd(small,semimid);
%nset2=mid;%imadd(mid,nset);

nodule_com= large;%imadd(nset, nset2);%small;%mid;%large;%imadd(mid,large);% imadd(small, semimid, mid, large);   ;%

nodule=nodule_com;
end

