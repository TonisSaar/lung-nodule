function[F] =nodule(grayImage)
[rows, columns] = size(grayImage);
D = im2bw(grayImage,0.005);

binaryImage = imfill(D, 'holes');
[~, numberOfBlobs] = bwlabel(binaryImage);

numberToExtract=(round(numberOfBlobs/1.1));
numberToExtract1=numberOfBlobs-numberToExtract;

numberToExtract = -numberToExtract;
smallnodule = Extractnodule(binaryImage, numberToExtract);

e = strel('disk',1);
E = imdilate(smallnodule,e);
[M,U] = bwboundaries(E,'noholes');
labeleImage=bwlabel(E);
stat = regionprops(U,E,'area','EquivDiameter');
metrics=zeros(1, 150);
metrics1=zeros(1, 150);
for H = 1:length(M)
    boundary = M{H};
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    area = stat(H).Area;
    %equidiameter
    metrics(H) =sqrt(4 * area / pi);
    %circularity or roundness
    metrics1(H)= 4*pi*area/perimeter^2;
    %EulerNumber
    %metrics1(H)=stat(H).EulerNumber;
end
allBlobround = metrics;
allBlobround1 = metrics1;
allowedIndexes = (allBlobround >4.5)&(allBlobround <8);
keeperIndex = find(allowedIndexes );
keepBlobImage1 = ismember(labeleImage, keeperIndex);
labeleImage1=bwlabel(keepBlobImage1);
allowedIndexes1 = (allBlobround1 >0.70);
keeperIndex1 = find(allowedIndexes1 );
keepBlobImage = ismember(labeleImage1, keeperIndex1);

largeBlob = Extractnodule(binaryImage, numberToExtract1);
maskImage = zeros(rows, columns, 'uint16'); % Initialize.
maskImage(largeBlob) = grayImage(largeBlob);
dm = im2bw(maskImage,0.007);
ls= strel('disk',1);
tq = imerode(dm,ls);
[B,L] = bwboundaries(tq,'noholes');
labeledImage=bwlabel(tq);
stats = regionprops(L,tq,'area');
metric=zeros(1, 150);
for k = 1:length(B)
    % obtain (X,Y) boundary coordinates corresponding to label 'k'
    boundary = B{k};
    % compute a simple estimate of the object's perimeter
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    % obtain the area calculation corresponding to label 'k'
    area = stats(k).Area;
    % compute the roundness metric
    metric(k) = 4*pi*area/perimeter^2;
end
allBlobroundness = metric;
%allBlobAreas = area;
allowablerIndexes = (allBlobroundness >0.35);
keeperIndexes = find(allowablerIndexes );
keeperBlobsImage = ismember(labeledImage, keeperIndexes);

X=keeperBlobsImage;
Ft=imadd(keepBlobImage,X);
binaryImage3 = imfill(Ft>0, 'holes');
maskerImage = zeros(rows, columns, 'uint16'); % Initialize.
maskerImage( binaryImage3) = grayImage( binaryImage3);
labImage=bwlabel(maskerImage);
s = regionprops(binaryImage3,  maskerImage,'MaxIntensity');
numObj = numel(s);
met=zeros(1,80);
for k = 1 :  numObj
    met(k) =s(k).MaxIntensity;
end
allBlo=met;
lowStd = find(allBlo>480);
kperBlobsImage1 = ismember(labImage, lowStd);
% % po=strel('disk',2);
% % tq1= imopen(kperBlobsImage1,po);
% % [Y,L] = bwboundaries(tq1,'noholes');
% % labeledImage=bwlabel(tq1);
% % stats1 = regionprops(L,tq1,'all');
% % metric=zeros(1, 300);
% % for k = 1:length(Y)
% %     area = stats1(k).Area;
% %     % compute the equidiameter metric
% %         metric(k) =sqrt(4 * area / pi);
% % end
% % allBlobroundness = (metric);
% % allowablerIndexes = (allBlobroundness >1);%&(allBlobroundness <20);
% % keeperIndexes = find(allowablerIndexes );
% % keeperBlobsImage = ismember(labeledImage, keeperIndexes);
 F=kperBlobsImage1;


















