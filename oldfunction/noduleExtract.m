function [F maskedImage] = noduleExtract( grayImage)
[rows, columns ] = size(grayImage);
binaryImage = grayImage<620;
borderImage = logical(binaryImage - imclearborder(binaryImage));
I2 = imsubtract(binaryImage, borderImage);
f = strel('rectangle', [29 19]);
io = imclose(I2,f);
ft = strel('ball', 2,0,4); %replace disk with diamond if required
io1 = imopen(io,ft);
gt=strel('disk', 7);
io2 = imerode(io1,gt);
gtt=strel('disk',4);
io3 = imdilate(io2,gtt);
[labeledImage numberOfBlobs] = bwlabel(io3);
blobMeasurements = regionprops(labeledImage, 'Area');
allAreas = [blobMeasurements.Area];
[sortedAreas sortIndices] = sort(allAreas, 'descend');
if (numel(sortIndices)>1)
    keeperIndexes = [sortIndices(1), sortIndices(2)];
    keeperBlobsImage = ismember(labeledImage, keeperIndexes);
    binaryImage3 = imfill(keeperBlobsImage>0, 'holes');
else %(numel(sortIndices)<1);
    [rows, columns ] = size(grayImage);
    binaryImage = grayImage < 255;
    
    borderImage = logical(binaryImage - imclearborder(binaryImage));
    grayImage(borderImage) = 0;
    
    f = strel('disk',6); %replace disk with diamond if required
    io = imopen(grayImage,f);
    
    binaryImage3 = (io> 50) & (io<620); % Threshold to isolate lung tissue
    
    [labeledImage numberOfBlobs] = bwlabel(binaryImage3);
    blobMeasurements = regionprops(labeledImage, 'Area');
    allAreas = [blobMeasurements.Area];
    [sortedAreas sortIndices] = sort(allAreas, 'descend');
    
    if (numel(sortIndices)>1)
        keeperIndexes = [sortIndices(1), sortIndices(2)];
        keeperBlobsImage = ismember(labeledImage, keeperIndexes);
        binaryImage3 = imfill(keeperBlobsImage>0, 'holes');
    end
end
maskedImage = zeros(rows, columns, 'uint16'); % Initialize.
e = strel('disk',1);%morphological operation using disk structuring element
% binaryImage3 = imerode(binaryImage3,e); %erosion process
binaryImage3 = imerode(binaryImage3,e); %erosion process
maskedImage(binaryImage3) = grayImage(binaryImage3);

F=nodule(maskedImage);

maskedImage(binaryImage3) = grayImage(binaryImage3);