function [ROIonlyLloyd,levels] = lloydQuantization(ROIonly,Ng)
% -------------------------------------------------------------------------
% function [ROIonlyLloyd,levels] = lloydQuantization(ROIonly,Ng)
% -------------------------------------------------------------------------
% DESCRIPTION: 
% This function computes Lloyd-Max quantization on the region of interest 
% (ROI) of an input volume.
% -------------------------------------------------------------------------
% REFERENCES:
% [1] Max, J. (1960). Quantizing for minimum distortion. IEEE Trans. Inf.
%     Theory, 6(1), 7–12.
% [2] Lloyd, S. (1982). Least Squanres Quantization in PCM. IEEE Trans.
%     Inf. Theory, IT-28(2),129–137.
% -------------------------------------------------------------------------
% INPUTS:
% - ROIonly: 3D array, with voxels outside the ROI set to NaNs.
% - Ng: Integer specifying the number of gray levels in the quantization.
% -------------------------------------------------------------------------
% OUTPUTS:
% - ROIonlyLloyd: Quantized input volume.
% - levels: Vector containing the quantized gray-level values in the ROI
%           (or reconstruction levels of quantization).
% -------------------------------------------------------------------------

ROIonly = double(ROIonly);
ROIonly = ROIonly./max(ROIonly(:));
qmax = max(ROIonly(:));
qmin = min(ROIonly(:));
ROIonly(isnan(ROIonly)) = qmax + 1;

[b,perm] = sort(reshape(ROIonly,[1 size(ROIonly,1)*size(ROIonly,2)*size(ROIonly,3)]));

d = b(find(b<=1));
if ~isreal(d)
d=real(d);
end

% [transitions,~] = lloyds(d,Ng); % LLoyd- algorithm



try
        [transitions,~] = lloyds(d,Ng); % LLoyd- algorithm
    catch err
        % "Error computing the Delaunay triangulation. The points may be
        % coplanar or collinear"['comm:lloyds:InvalidTRAINING_SET']
        %
        % Just exit the function leaving the input segmentation untouched
        if (strcmp(err.identifier,'comm:lloyds:InvalidTRAINING_SET'))
           d=[0.6734    0.7058    0.8122    0.8937    0.8986    0.9089    0.9633    1.0000];
            [transitions,~] = lloyds(d,Ng);
            %         else if (strcmp(err.identifier,'MATLAB:convhull:NotEnoughPtsConvhullErrId'))||(strcmp(err.identifier,'MATLAB:convhull:ThreePtsForTriErrId'))
            %             % display any other errors as usual
            %             volumeH = volumeH1;
        else
            rethrow(err);
        end
        %end
end

temp = [qmin,transitions,qmax];
transitions = temp;

sortEqual = zeros(1,size(ROIonly,1)*size(ROIonly,2)*size(ROIonly,3));

sortEqual(find(b <= transitions(2) & b >= transitions(1))) = 1;
for i = 2:Ng
    sortEqual(find(b <= transitions(i+1) & b > transitions(i))) = i;
end

ROIonlyLloyd = zeros(1,size(ROIonly,1)*size(ROIonly,2)*size(ROIonly,3));
ROIonlyLloyd(perm(1:end)) = sortEqual(1:end);

ROIonlyLloyd = reshape(ROIonlyLloyd,[size(ROIonly,1),size(ROIonly,2),size(ROIonly,3)]);
ROIonlyLloyd(ROIonly==2) = NaN;

levels = 1:Ng;

end