function [aucCSH] = getAUCCSH(ROIonlyCT)
% -------------------------------------------------------------------------
% function [aucCSH] = getAUCCSH(ROIonlyCT)
% -------------------------------------------------------------------------
% DESCRIPTION: 
% This function computes the area under the curve of the cumulative 
% SUV-volume histogram from the region of interest (ROI) of an input CT 
% volume.
% -------------------------------------------------------------------------
% REFERENCE:
% [1] van Velden, F. H. P. et al. (2011). Evaluation of a cumulative
%     SUV-volume histogram method for parameterizing heterogeneoous
%     intratumoural FDG uptake in non-small cell lung cancer. European 
%     Journal of Nuclear Medicine and Molecular Imaging, 38(9), 1636-1647.
% -------------------------------------------------------------------------
% INPUTS:
% - ROIonlyCT: 3D array representing the CT volume in SUV format, with 
%               voxels outside the ROI set to NaNs. 
% -------------------------------------------------------------------------
% OUTPUTS:
% - aucCSH: Area under the curve of the cumulative SUV-volume histogram
%           describing the percentage of total volume of the ROI above a 
%           percentage threshold of maximum SUV.
% -------------------------------------------------------------------------


nBins = 1000; % By default.

outliers = find(ROIonlyCT > mean(ROIonlyCT(~isnan(ROIonlyCT(:)))) + 3*std(ROIonlyCT(~isnan(ROIonlyCT(:)))));
goodVoxels = find(ROIonlyCT <= mean(ROIonlyCT(~isnan(ROIonlyCT(:)))) + 3*std(ROIonlyCT(~isnan(ROIonlyCT(:)))));
ROIonlyCT(outliers) = mean(ROIonlyCT(goodVoxels));

ROIonlyCT = ROIonlyCT-min(ROIonlyCT(:));
ROIonlyCT = ROIonlyCT./max(ROIonlyCT(:));
volume = ROIonlyCT(~isnan(ROIonlyCT));
nVoxel = numel(volume);

bins = hist(volume,nBins);
CSH = fliplr(cumsum(fliplr(bins))./nVoxel);
aucCSH = sum(CSH./nBins);

end