xtt=(noduleVol{1,1}{1,3});
[x, y, z] = size(noduleVol{1,1}{3,1});
blbVol= (noduleVol{1,1}{3,1});
blobImg=(noduleVol{1,1}{3,2});
xt=xtt(1,1);
yt=xtt(1,2);
zt=xtt(1,3);
xnew=x*xt; ynew=y*yt; znew=z*zt;
mask = (blobImg);%~isnan
 numberVoxel = sum(mask(:));
 volume_1 = numberVoxel * xt * yt * zt;
 dia_size=nthroot(volume_1,3);
 smm=(numel(mask(:)));
compactness=numberVoxel/smm;
perimeter = bwperim(mask,26);
nPoints = length(find(perimeter));
compactness_a=numberVoxel/nPoints;
% if ~isempty(ROIonly)
%     if (numel(size(ROIonly)) >= 3)
%         mask = (ROIonly); % Find mask covering the ROI~isnan
%         volumeH1 = sum(mask(:));
% ISOTROPIC RESAMPLING
sFactor = zt/xt; % scaling factor
mask_1 = imresize3d(mask,[],[round(double(size(mask,1))),round(double(size(mask,2))),round(double(size(mask,3))*sFactor)],'nearest','fill');
numberVoxel_1 = sum(mask_1(:));
smm_1=(numel(mask_1(:)));
compactness_1=numberVoxel_1/smm_1;
volume_2 = numberVoxel_1;
dia_size_1=nthroot(volume_2,3);
perimeter_1= bwperim(mask_1,26);
nPoints_1 = length(find(perimeter_1));
compactness_2=numberVoxel_1/nPoints_1;
%[sizeROI_1] = getSize(mask_1,xt,zt);
%     end
% end394143
%regionprops
 