function [solidity] = getSolidity(ROIonly,pixelW,sliceS)
% -------------------------------------------------------------------------
% function [solidity] = getSolidity(ROIonly,pixelW,sliceS)
% -------------------------------------------------------------------------
% DESCRIPTION: 
% This function computes the solidity metric of the region of interest 
% (ROI) of an input volume.
% -------------------------------------------------------------------------
% INPUTS:
% - ROIonly: 3D array, with voxels outside the ROI set to NaNs.
% - pixelW: Pixel width, or in-plane resolution, in mm.
% - sliceS: Slice spacing, in mm.
% -------------------------------------------------------------------------
% OUTPUTS:
% - solidity: Ratio of the number of voxels in the ROI to the number of 
%             voxels in the 3D convex hull of the ROI (smallest polyhedron 
%             containing the ROI).
% -------------------------------------------------------------------------

if ~isempty(ROIonly)
            if (numel(size(ROIonly)) >= 3)
mask = (ROIonly); % Find mask covering the ROI~isnan
volumeH1 = sum(mask(:));
% ISOTROPIC RESAMPLING
sFactor = sliceS/pixelW; % scaling factor
mask = imresize3d(mask,[],[round(double(size(mask,1))),round(double(size(mask,2))),round(double(size(mask,3))*sFactor)],'nearest','fill');
            end
end
% SOLIDITY COMPUTATION
perimeter = bwperim(mask,18);
nPoints = length(find(perimeter));
X = zeros(nPoints,1); Y = zeros(nPoints,1); Z = zeros(nPoints,1);
count = 1;
for i = 1:size(perimeter,3)
    [row,col] = find(perimeter(:,:,i));
    p = length(row);
    if p > 0
        X(count:count+p-1,1) = col(1:end);
        Y(count:count+p-1,1) = row(1:end);
        Z(count:count+p-1,1) = i;
        count = count + p;
    end
end

try
        [~,volumeH] = convhull(X,Y,Z);
    catch err
        % "Error computing the Delaunay triangulation. The points may be
        % coplanar or collinear"
        %
        % Just exit the function leaving the input segmentation untouched
        if (strcmp(err.identifier,'MATLAB:convhull:EmptyConvhull3DErrId'))
            volumeH = volumeH1;
        else if (strcmp(err.identifier,'MATLAB:convhull:NotEnoughPtsConvhullErrId'))||(strcmp(err.identifier,'MATLAB:convhull:ThreePtsForTriErrId'))
            % display any other errors as usual
            volumeH = volumeH1;
            else
                rethrow(err);
            end
        end
end
volumeROI = sum(mask(:));
solidity = volumeROI/volumeH;
end