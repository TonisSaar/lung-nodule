function [SUVmax,SUVpeak,SUVmean,aucCSH] = getSUVmetrics(ROIonlyCT)
% -------------------------------------------------------------------------
% function [SUVmax,SUVpeak,SUVmean,aucCSH] = getSUVmetrics(ROIonlyCT)
% -------------------------------------------------------------------------
% DESCRIPTION: 
% This function computes SUVmax, SUVpeak and SUVmean, AUC-CSH and Percent 
% Inactive metrics from the region of interest (ROI) of an input CT volume.
% -------------------------------------------------------------------------
% INPUTS:
% - ROIonlyCT: 3D array representing the CT volume in SUV format, with 
%               voxels outside the ROI set to NaNs. 
% -------------------------------------------------------------------------
% OUTPUTS:
% - SUVmax: Maximum SUV of the ROI.
% - SUVpeak: Average of the voxel with maximum SUV within the ROI and its 
%            26 connected neighbours.
% - SUVmean: Average SUV value of the ROI.
% - aucCSH: Area under the curve of the cumulative SUV-volume histogram
%           describing the percentage of total volume of the ROI above a 
%           percentage threshold of maximum SUV.
%           (van Velden et al., Eur J Nucl Med Mol Imaging 38(9), 2011).
% -------------------------------------------------------------------------

% Initialization
ROIonlyCT = padarray(ROIonlyCT,[1 1 1],NaN);

% SUVmax
[SUVmax,indMax] = max(ROIonlyCT(:));

% SUVpeak (using 26 neighbors around SUVmax)
[indMaxX,indMaxY,indMaxZ] = ind2sub(size(ROIonlyCT),indMax);
connectivity = getneighbors(strel('arbitrary',conndef(3,'maximal')));
nPeak = length(connectivity);
neighborsMax = zeros(1,nPeak);
for i=1:nPeak
    neighborsMax(i) = ROIonlyCT(connectivity(i,1)+indMaxX,connectivity(i,2)+indMaxY,connectivity(i,3)+indMaxZ);
end
SUVpeak = mean(neighborsMax(~isnan(neighborsMax)));

% SUVmean
SUVmean=mean(ROIonlyCT(~isnan(ROIonlyCT)));

% AUC-CSH
[aucCSH] = getAUCCSH(ROIonlyCT);

end