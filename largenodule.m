%%%% for nodules >10mm size %%%
function n4=largenodule(set,grayImage, numberToExtract4) 
largenodule =  numberToExtract4;
[rows, columns] = size(grayImage);
masklImage = zeros(rows, columns, 'uint16'); % Initialize.
masklImage(largenodule) = grayImage(largenodule);
dm = im2bw(masklImage,0.007);

tq=bwmorph(dm,'branchpoints');
if   or(min(set.spacing)<=0.80,max(set.spacing)>1.50) %max(set.spacing)>=1.5%length(set.index)<= 140
    ts= strel('disk',2);
    ts1 = imerode(tq,ts);
    [B,L] = bwboundaries(ts1,'noholes');
    labeledImage=bwlabel(ts1);
    stats = regionprops(L,ts1,'all');
    metric=size(B);%(1, 15);
% metrice=zeros(1, 150);
    for k = 1:length(B)
        perimeter = ConvexPerim(k);
    %     % obtain the area calculation corresponding to label 'k'
         area = stats(k).Area;
    %     % compute the roundness metric
        metric(k)=(4*pi*area)/perimeter ^2;
    %     % compute the Eccentricity metric
    %     metrice(k) = stats(k).Eccentricity;
    end
    allBlobroundness = metric;
    t=max(allBlobroundness);
    if (t <=100)%|| max(allBlobround) >13 **
    allowablerIndexes = (allBlobroundness>=9);
    allowablerIndexes_UN = (allBlobroundness<9);
    elseif (t>100)&&(t<=300)
    allowablerIndexes = (allBlobroundness>=85);
    allowablerIndexes_UN = (allBlobroundness<85);
    elseif(t>300)&&(t<=500)
    allowablerIndexes = (allBlobroundness>=190);
    allowablerIndexes_UN= (allBlobroundness<190);
    else
    allowablerIndexes = (allBlobroundness>=380);
    allowablerIndexes_UN = (allBlobroundness>100)&(allBlobroundness<340);
    end
    keeperIndexes = find(allowablerIndexes );
    keeperIndexes_UN = find(allowablerIndexes_UN);

    keeperBlobsImage1 = ismember(labeledImage, keeperIndexes);
    keeperBlobsImage1_UN = ismember(labeledImage, keeperIndexes_UN);
    UN=keeperBlobsImage1_UN;
    ls= strel('disk',2);
    ts2 = imdilate(keeperBlobsImage1,ls);
    ts12=imadd(UN,ts2);
else
    [B,L] = bwboundaries(tq,'noholes');
    labeledImage=bwlabel(tq);
    stats = regionprops(L,tq,'all');
    metric=size(B);%(1, 15);
    % metrice=zeros(1, 150);
    for k = 1:length(B)
        perimeter = ConvexPerim(k);
        %     % obtain the area calculation corresponding to label 'k'
        area = stats(k).Area;
        %     % compute the roundness metric
        metric(k)=(4*pi*area)/perimeter ^2;
        %     % compute the Eccentricity metric
        %     metrice(k) = stats(k).Eccentricity;
    end
    allBlobroundness = metric;
    t=max(allBlobroundness);
    if (t <=100)%|| max(allBlobround) >13 **
    allowablerIndexes = (allBlobroundness>=9);
    allowablerIndexes_UN = (allBlobroundness<9);
    elseif (t>100)&&(t<=300)
    allowablerIndexes = (allBlobroundness>=85);
    allowablerIndexes_UN = (allBlobroundness<85);
    elseif(t>300)&&(t<=500)
    allowablerIndexes = (allBlobroundness>=190);
    allowablerIndexes_UN= (allBlobroundness<190);
    else
    allowablerIndexes = (allBlobroundness>=380);
    allowablerIndexes_UN = (allBlobroundness>100)&(allBlobroundness<340);
    end
    keeperIndexes = find(allowablerIndexes );
    keeperIndexes_UN = find(allowablerIndexes_UN);

    keeperBlobsImage1 = ismember(labeledImage, keeperIndexes);
    keeperBlobsImage1_UN = ismember(labeledImage, keeperIndexes_UN);
    UN=keeperBlobsImage1_UN;
    ls= strel('disk',2);
    tq1 = imdilate(keeperBlobsImage1,ls);
    ts= strel('disk',1);
    ts2 = imerode(tq1,ts);
    ts12=imadd(UN,ts2);
end
n4=ts12;%largenodule;
end