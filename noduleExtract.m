function[nodule] =noduleExtract(set,grayImage)

%load('lidc5.mat')
%set = lidc(1);

%grayImage=io;
[rows, columns] = size(grayImage);
D = im2bw(grayImage,0.005);
binaryImage = imfill(D, 'holes');

[M1,U1] = bwboundaries(binaryImage,'noholes');
[labeledImage, numberOfBlobs] = bwlabel(binaryImage);
blobMeasurements = regionprops(U1,labeledImage, 'Area');
allarea=size(M1);%=zeros(1, 400);
mt=size(allarea);%zeros(1, 400);

for h1=1:length(M1)
    % boundary = M1{h1};
    allarea(h1)=blobMeasurements(h1).Area;
    tq= allarea(h1).*((set.spacing(1)*set.spacing(2)));
    mt(h1)=sqrt((4*tq)/pi);
end
%end

n2=mt;
number1=(n2>=1.5)&(n2<=3);
keeperIndex1 = find(number1);
numberToExtract1 = ismember(labeledImage, keeperIndex1);

number2=(n2>3)&(n2<=6);
keeperIndex2 = find(number2);
numberToExtract2 = ismember(labeledImage, keeperIndex2);
number3=(n2>6)&(n2<=10);
keeperIndex3 = find(number3);
numberToExtract3 = ismember(labeledImage, keeperIndex3);
number4=(n2>10);%;%&
keeperIndex4 = find(number4);
numberToExtract4 = ismember(labeledImage, keeperIndex4);

small=smallnodule(numberToExtract1);%numberToExtract1;
small1=int16(small);
semimid=semimidnodule(numberToExtract2);
semimid1=int16(semimid);
mid=midnodule(numberToExtract3);
mid1= int16(mid);
large=largenodule(set, grayImage,numberToExtract4);
large1=int16(large);
%K = imlincomb(1,small,1,J,'uint16')
nset=imadd(small1,semimid1);
nset2=imadd(mid1,large1);
nodule_com= imadd(nset2, nset);%small;%mid;%large;%imadd(mid,large);% imadd(small, semimid, mid, large);   ;%
%nodule=nodule_com;




[B1,L1] = bwboundaries(nodule_com,'noholes');
[lImage, numbs]=bwlabel(nodule_com);
statsMid = regionprops(L1,lImage,'area');
mtrr=size(B1);
for kM = 1:length(B1)
    % boundary = M1{h1};
    allarea(kM)=statsMid(kM).Area;
    tqrr= allarea(kM).*((set.spacing(1)*set.spacing(2)));
    mtrr(kM)=sqrt((4*tqrr)/pi);
end
nn=mtrr;

numbe_2=(nn>1.6)&(nn<=2);%
keeperInde_2 = find(numbe_2);
numberToExt_2 = ismember(lImage, keeperInde_2);
mid_nodu =  numberToExt_2;
% tm_s= strel('disk',1);
% tms_1 = imerode(mid_nodu,tm_s);
tms_1=single(mid_nodu);
number_2=(nn>2)&(nn<=8);%
keeperIndex_2 = find(number_2);
numberToExtract_2 = ismember(lImage, keeperIndex_2);
mid_nodule =  numberToExtract_2;
t_s= strel('disk',1);
ts_1 = imerode(mid_nodule,t_s);
ts_1=single(ts_1);

num_2=(nn>8)&(nn<=20);
keeper_2 = find(num_2);
numberTo_2 = ismember(lImage, keeper_2);
mid_nod =  numberTo_2;
t_ss= strel('disk',2);
tss_1 = imerode(mid_nod,t_ss);
% t_sss= strel('disk',1);
% tsss_1 = imdilate(tss_1,t_sss);
tss_1=single(tss_1);


num_3=(nn>20)&(nn<=32);
keeper_3 = find(num_3);
numberTo_3 = ismember(lImage, keeper_3);
mid_nod_3 =  numberTo_3;
t_ss_3= strel('disk',3);
tss_3 = imerode(mid_nod_3,t_ss_3);
% t_sss= strel('disk',1);
% tsss_1 = imdilate(tss_1,t_sss);
tss_3=single(tss_3);


num_4=(nn>32)&(nn<=47.5);
keeper_4 = find(num_4);
numberTo_4 = ismember(lImage, keeper_4);
mid_nod_4 =  numberTo_4;
t_ss_4= strel('disk',4);
tss_4 = imerode(mid_nod_4,t_ss_4);
% t_sss= strel('disk',1);
% tsss_1 = imdilate(tss_1,t_sss); if we make 30-47.5 and disk size 6 better
% results but need to sure it%%%%%%
tss_4=single(tss_4);


mtl_1=imadd(tss_4,tss_3);

mtl=imadd(ts_1,tms_1);
mtl=single(mtl);
mtl_2=imadd(mtl_1, mtl);
nodule_1=imadd(tss_1,mtl_2);

tqh=bwmorph(nodule_1,'diag');
 t_ssss= strel('disk',1);
 tssss_1 = imdilate(tqh,t_ssss);
nodule=tssss_1;%nodule_1;%
end
% switch