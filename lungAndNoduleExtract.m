function [F maskedImage]= lungAndNoduleExtract(set, grayimage)

%%%%%%lung extraction 
maskedImage=lungextract(grayimage);

%%%%%%%%%%nodule extraction
F=noduleExtract(set, maskedImage);
end
